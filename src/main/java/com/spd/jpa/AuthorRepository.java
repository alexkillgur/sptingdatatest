package com.spd.jpa;

import com.spd.entity.Author;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Igor on 11.12.2016.
 */
public interface AuthorRepository extends CrudRepository<Author, Integer> {

//    @Query("Select * from AUTHOR where name = ?1")
    Author findOneByName(String name);

    List<Author> findPleaseAuthorsByDocumentsTitleLike(String nameLike);
    List<Author> getDistinctAuthorsByDocuments_Title_Like(String nameLike);

    @Query("select a from Author a where a.name = ?1")
    Author getAuthorByQueryWithName(String name);

    @Query("select a from Author a where a.name = :name")
    Author getAuthorByQueryWithParamName(@Param("name") String name);

    Author findByNamedQuery(String n);

    @Procedure
    Integer plus1(@Param("arg") Integer arg);

    @Procedure("plus1inout")
    Integer explicitlyNamedPlus1inout(Integer arg);
}
