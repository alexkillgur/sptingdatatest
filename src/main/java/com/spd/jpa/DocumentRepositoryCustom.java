package com.spd.jpa;

import com.spd.entity.Document;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

/**
 * Created by Igor on 13.12.2016.
 */
//@NoRepositoryBean
public interface DocumentRepositoryCustom {
    Optional<Document> myCustomMethodForADoc();
}
