package com.spd.testdata;

import com.spd.entity.Author;
import com.spd.entity.Document;
import com.spd.jpa.AuthorRepository;
import com.spd.jpa.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Igor on 11.12.2016.
 */

@Component
public class TestDataGenerator {
    @Autowired
    AuthorRepository authorRepository;

    @Autowired
    DocumentRepository documentRepository;

    public void generateData(){
        Author igor = new Author();
        igor.setName("Igor");
        authorRepository.save(igor);

        Author oleg = new Author();
        oleg.setName("Oleg");
        authorRepository.save(oleg);

        List<Document> olegDocs = generateDocuments(oleg, 10);
        documentRepository.save(olegDocs);


        Author dima = new Author();
        dima.setName("Dmitrii");
        authorRepository.save(dima);

        List<Document> dimaDocs = generateDocuments(dima, 5);
        documentRepository.save(dimaDocs);

    }

    private List<Document> generateDocuments(Author oleg, int count) {
        return Stream.iterate(0, n  ->  n  + 1)
                .limit(count)
                .map((n) -> generateDocument(oleg))
                .collect(Collectors.toList());
    }

    private Document generateDocument(Author author) {
        Document doc1 = new Document();
        doc1.setTitle("Document " + new Random().nextInt());
        doc1.setText("Document author : " + author.getName());
        doc1.setAuthor(author);
        return doc1;
    }


}
