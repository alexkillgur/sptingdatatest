package com.spd;

import com.spd.entity.Author;
import com.spd.entity.Document;
import com.spd.jpa.AuthorRepository;
import com.spd.jpa.DocumentRepository;
import com.spd.jpa.JpaSpecificationRepository;
import com.spd.jpa.QueryByExampleExecutorRepository;
import com.spd.projection.DocumentTitleOnly;
import com.spd.testdata.TestDataGenerator;
import org.hsqldb.util.DatabaseManagerSwing;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;
import java.util.function.Consumer;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class SpringdataApplicationTests {
    @Autowired
    private TestDataGenerator testDataGenerator;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private JpaSpecificationRepository jpaSpecificationRepository;

    @Autowired
    private QueryByExampleExecutorRepository exampleExecutorRepository;

    @BeforeClass
    public static void getDbManager(){
//        DatabaseManagerSwing.main(
//                new String[] { "--url", "jdbc:hsqldb:mem:springdatatest", "--user", "sa", "--password", ""});
    }

    @Before
    public void generateData(){
//        testDataGenerator.generateData();
    }

	@Test
	public void readAllAuthors() {
        Iterable<Author> authors = authorRepository.findAll();
        for (Author author : authors) {
            System.out.println(author.getId() + " : " + author.getName());
        }
    }

    @Test
    public void findAuthorByName () {
        System.out.println();
        System.out.println("Find author by name");
        Author igor = authorRepository.findOneByName("igor");
        System.out.println(igor);
//        assertNotNull(igor);
//        assertEquals("Igor", igor.getName());
    }

    @Test
    @Transactional
    public void findAuthorByDocLike(){
        System.out.println();
        System.out.println("Find authors by document Like");
        List<Author> authorList = authorRepository.findPleaseAuthorsByDocumentsTitleLike("%-%");
        authorList.stream().forEach(print());

        System.out.println();
        System.out.println("Find DISTINCT authors by document Like");
        List<Author> authorList2 = authorRepository.getDistinctAuthorsByDocuments_Title_Like("%-%");
        authorList2.stream().forEach(print());
    }


    @Test
    @Transactional
    public void testPageableAndSort(){
        PageRequest pageable = new PageRequest(1, 3);
        Page<Document> all = documentRepository.findAll(pageable);
        all.forEach(print());

        System.out.println("-- Sorted ");
        Sort sort = new Sort(Sort.Direction.DESC, "text");
        List<Document> documentList = documentRepository.findAll(sort);
        documentList.forEach(print());

        System.out.println("-- Page and Sorted asc ");
        PageRequest pageAndSort = new PageRequest(1, 3, Sort.Direction.ASC, "text");
        Slice<Document> documentList2 = documentRepository.findAll(pageAndSort);
        documentList2.forEach(print());

//        Sort sort2 = new Sort(Sort.Direction.ASC, "text");
//        List<Document> documentList2 = documentRepository.findAll(pageable, sort);
//        documentList2.forEach(print());
    }

    @Test
    public void customRepository(){
        documentRepository.myCustomMethodForADoc()
            .ifPresent(print());
    }

    private Consumer print() {
        return System.out::println;
    }


    /*@Test
    public void queryDsl(){

    }*/

    @Test
    public void query(){
        //Query
        Author author = authorRepository.getAuthorByQueryWithName("Igor");
        System.out.println(author);

        //Query with param
        Author author2 = authorRepository.getAuthorByQueryWithName("Igor");
        System.out.println(author2);

        //Named query
        Author author3 = authorRepository.findByNamedQuery("Igor");
        System.out.println(author2);

    }

    @Test
    public void modifyingQuery(){
        documentRepository.updateDocumentTitle(" prefix ");
        Page<Document> all = documentRepository.findAll(new PageRequest(0, 1));
        all.getContent().forEach(print());
    }

    @Test
    public void projection(){
        List<DocumentTitleOnly> documentTitleOnly = documentRepository.findByTitleEndsWith("1");
        documentTitleOnly.forEach(
                d -> {
                    System.out.println(d.getTitle());
                    System.out.println(d.getTitleWithId());
                }
        );
    }


    @Test
    public void storedProcedure(){
        System.out.println(
            authorRepository.explicitlyNamedPlus1inout(10)
        );
        System.out.println(
            authorRepository.plus1(20)
        );
    }

    @Test
    public void testSpecification(){
        Specification<Document> spec = new Specification<Document>() {
            @Override
            public Predicate toPredicate(Root<Document> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                return cb.equal(root.join("author").get("name"), "oleg");
            }
        };
        List<Document> all = jpaSpecificationRepository.findAll(spec);
        all.forEach(print());

        System.out.println("=========");



        List<Document> all2 = jpaSpecificationRepository.findAll((root, query, cb) -> {
            return cb.equal(root.join("author").get("name"), "oleg");
        });
        all2.forEach(print());

        System.out.println("=========");

        List<Document> all22 = jpaSpecificationRepository.findAll(Specifications.where(spec)
                .or((root, query, cb) -> {
                    return cb.equal(root.join("author").get("name"), "dima");
                })
        );
        all22.forEach(print());
    }

    @Test
    public void exampleExecutorRepository(){
        Author author = new Author();
        author.setName("igor");
        Example<Author> example = Example.of(author);
        Iterable<Author> all = exampleExecutorRepository.findAll(example);
        all.forEach(print());


        System.out.println("-----------------");
        Author author2 = new Author();
        author2.setName("o");
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("name", match -> match.contains());
        Example<Author> example2 = Example.of(author2, matcher);
        Iterable<Author> all2 = exampleExecutorRepository.findAll(example2);
        all2.forEach(print());
    }
}
